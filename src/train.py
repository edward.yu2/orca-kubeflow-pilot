import argparse
import random
import numpy as np
import torch
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader, random_split
from model import Classifier, train_tfm

def count_labels(dataset):
    sum = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0}
    for d in dataset:
        sum[d[1].item()] += 1
    return sum

def train():
    parser = argparse.ArgumentParser()
    parser.add_argument("-cp", "--checkpoint", type=str, help="checkpoint path and base name to save model", default='checkpoints/image_qty_classifier')
    parser.add_argument("-d", "--data", type=str, help="training data path", default='data/train')
    parser.add_argument("-b", "--batch_size", type=int, help="batch size", default=64)
    parser.add_argument("-e", "--epochs", type=int, help="epochs", default=20)
    parser.add_argument("-r", "--seed", type=int, help="random seed", default=123)
    parser.add_argument("-o", "--output", type=str, help="output path")
    args = parser.parse_args()

    seed = args.seed
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    
    data_set = ImageFolder(root=args.data, transform=train_tfm, target_transform=transforms.Lambda(lambda x: torch.tensor(x)))

    train_size = int(0.6 * len(data_set))
    valid_size = int(0.5 * (len(data_set) - train_size))
    test_size = len(data_set) - train_size - valid_size
    train_set, valid_set, _ = random_split(data_set, [train_size, valid_size, test_size], generator=torch.Generator().manual_seed(seed))
    print(f'training size: {train_size:d}, validation size: {valid_size:d}, test size: {test_size:d}')

    train_loader = DataLoader(train_set, batch_size=args.batch_size, shuffle=True)
    valid_loader = DataLoader(valid_set, batch_size=args.batch_size, shuffle=False)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    model = Classifier().to(device)
    model.device = device

    weights = [1.5, 1.2, 1., 1., 1.5, 0.1]
    label_nums = count_labels(train_set)
    for c in range(1, len(label_nums)):
        weights[c] *= label_nums[0]/label_nums[c]
    print('loss weights:', weights)
    
    history = model.fit(train_loader, valid_loader, weights, device=device, cp_path=args.checkpoint, epochs=args.epochs, output=args.output)


if __name__ == '__main__':
    train()

# python /app/src/train.py -cp "/app/checkpoints/image_qty_classifier" -d "/app/data/train" -b 64 -e 20