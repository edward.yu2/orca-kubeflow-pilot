import numpy as np
import torch
import torch.nn as nn
import torchvision.transforms as transforms
from torchvision import models
from tqdm.auto import tqdm

train_tfm = transforms.Compose([
    transforms.Resize((224,224)),
    transforms.ToTensor(),
    transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    ),
])

reverse_tfm = transforms.Compose([
    transforms.Normalize(
        mean=[0., 0., 0.],
        std=[1/0.229, 1/0.224, 1/0.225]
    ),
    transforms.Normalize(
        mean=[-0.485, -0.456, -0.406],
        std=[1., 1., 1.]
    ),
])


class Classifier(nn.Module):
    def __init__(self, dropout=0.2, pretrained=True):
        super(Classifier, self).__init__()
        self.model = models.convnext_tiny(pretrained=pretrained)
        in_features = self.model.classifier[2].in_features
        print('#of features:', in_features)

        self.model.classifier[2] = nn.Sequential(
            nn.BatchNorm1d(in_features),
            nn.Dropout(dropout),
            nn.Linear(in_features, 256),
            nn.BatchNorm1d(256),
            nn.LeakyReLU(),
            nn.Dropout(dropout),
            nn.Linear(256, 128),
            nn.BatchNorm1d(128),
            nn.LeakyReLU(),
            nn.Dropout(dropout),
            nn.Linear(128, 6),
            #nn.LogSoftmax(dim=1),
        )

    def forward(self, x):
        # input (x): [batch_size, 3, 224, 224]
        # output: [batch_size, 5]

        x = self.model(x)
        return x

    def fit(
        self, 
        training_loader, validation_loader, class_weights, label_smoothing=0.005, device='cpu',
        optimizer=None, scheduler=None, cp_path=None, epochs=20, verbose=1, output=None
    ):
        history = {'train_loss': [], 'train_acc': [], 'valid_loss': [], 'valid_acc': []}
        if optimizer is None:
            optimizer = torch.optim.AdamW(self.parameters(), lr=0.0002, weight_decay=1e-5)
        if scheduler is None:
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=2)
        
        criterion = nn.CrossEntropyLoss(weight=torch.tensor(class_weights).to(device), label_smoothing=label_smoothing)
        valid_criterion = criterion #nn.CrossEntropyLoss()
        regre_criterion = nn.L1Loss()
        regre_factor = 0.8

        if output is None:
            output = f'{cp_path}_best-acc.pt'
        
        cp = {
            'epoch': 0,
            'train_loss': 0.,
            'train_acc': 0.,
            'valid_loss': 999.,
            'valid_acc': 0.,
            'model_state_dict': None,
            'opt_state_dict': None,
        }

        best_loss = 999.
        best_acc = 0.

        for epoch in range(epochs):
            train_losses = []
            train_accs = []
            
            self.train()
            for batch in tqdm(training_loader, disable=(verbose <= 0)):
                imgs, labels = batch
                logits = self.forward(imgs.to(device))
                loss = criterion(logits, labels.to(device))
                pred = logits.argmax(dim=-1)
                acc = (pred == labels.to(device)).float().mean()
                
                loss += regre_criterion(pred.type(torch.float), labels.to(device))*regre_factor

                train_losses.append(loss.item())
                train_accs.append(acc.item())

                optimizer.zero_grad()
                loss.backward()
                grad_norm = nn.utils.clip_grad_norm_(self.parameters(), max_norm=10)
                optimizer.step()

            avg_train_loss = np.mean(train_losses)
            avg_train_acc = np.mean(train_accs)
            history['train_loss'].append(avg_train_loss)
            history['train_acc'].append(avg_train_acc)
            if verbose > 0:
                print(f"[ Train | {epoch + 1:03d}/{epochs:03d} ] loss = {avg_train_loss:.5f}, acc = {avg_train_acc:.5f}")

            valid_losses = []
            valid_accs = []
            self.eval()
            for batch in tqdm(validation_loader, disable=(verbose <= 0)):
                imgs, labels = batch
                with torch.no_grad():
                    logits = self.forward(imgs.to(device))
                loss = valid_criterion(logits, labels.to(device))
                pred = logits.argmax(dim=-1)
                acc = (pred == labels.to(device)).float().mean()
                
                loss += regre_criterion(pred.type(torch.float), labels.to(device))*regre_factor

                valid_losses.append(loss.item())
                valid_accs.append(acc.item())

            avg_valid_loss = np.mean(valid_losses)
            avg_valid_acc = np.mean(valid_accs)
            history['valid_loss'].append(avg_valid_loss)
            history['valid_acc'].append(avg_valid_acc)
            scheduler.step(avg_valid_loss)
            if verbose > 0:
                print(f"[ Valid | {epoch + 1:03d}/{epochs:03d} ] loss = {avg_valid_loss:.5f}, acc = {avg_valid_acc:.5f}")
            
            best_changed = False
            if avg_valid_loss < best_loss:
                best_loss = avg_valid_loss
                best_changed = True
                best_cp = f'{cp_path}_best-loss.pt'
            if avg_valid_acc > best_acc:
                best_acc = avg_valid_acc
                best_changed = True
                best_cp = output
            if best_changed:
                cp['valid_loss'] = avg_valid_loss
                cp['valid_acc'] = avg_valid_acc
                cp['train_loss'] = avg_train_loss
                cp['train_acc'] = avg_train_acc
                cp['epoch'] = epoch+1
                cp['model_state_dict'] = self.state_dict()
                cp['opt_stat_dict'] = optimizer.state_dict()
                torch.save(cp, best_cp)

        return history

    def evaluation(self, dataloader, device, verbose=1):
        criterion = criterion = nn.CrossEntropyLoss()
        losses = []
        accs = []
        self.eval()
        for batch in tqdm(dataloader, disable=(verbose <= 0)):
            imgs, labels = batch
            with torch.no_grad():
                logits = self.forward(imgs.to(device))
            loss = criterion(logits, labels.to(device))
            acc = (logits.argmax(dim=-1) == labels.to(device)).float().mean()
            losses.append(loss.item())
            accs.append(acc.item())
        return np.mean(losses), np.mean(accs)


    def predict(self, x, device):
        self.eval()
        with torch.no_grad():
            logits = self.forward(x.to(device))
        probs = nn.Softmax(dim=-1)(logits)
        predictions = probs.max(dim=-1)
        return predictions.indices, predictions.values

def restore_checkpoint(cp_path:str, device:str) -> Classifier:
    cp = torch.load(cp_path, map_location=torch.device(device))
    model = Classifier(pretrained=False).to(device)
    model.device = device
    model.load_state_dict(cp['model_state_dict'])
    print(f"restore to epoch: {cp['epoch']:d}, training loss: {cp['train_loss']:.4f}, training accurate: {cp['train_acc']:.4f}, validation loss: {cp['valid_loss']:4f}, validation accurate: {cp['valid_acc']:.4f}")
    return model