import argparse
import random
import os
import numpy as np
import torch
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader, random_split
from dvclive import Live
from model import restore_checkpoint, train_tfm

def evaluate():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", type=str, help="model path", default='checkpoints/image_qty_classifier_best-acc.pt')
    parser.add_argument("-d", "--data", type=str, help="data path", default='data/train')
    parser.add_argument("-b", "--batch_size", type=int, help="batch size", default=32)
    parser.add_argument("-r", "--seed", type=int, help="random seed", default=123)
    parser.add_argument("-e", "--eval_path", type=str, help="evaluation metrics path", default='evaluation')
    args = parser.parse_args()

    seed = args.seed
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    
    data_set = ImageFolder(root=args.data, transform=train_tfm, target_transform=transforms.Lambda(lambda x: torch.tensor(x)))

    train_size = int(0.6 * len(data_set))
    valid_size = int(0.5 * (len(data_set) - train_size))
    test_size = len(data_set) - train_size - valid_size
    _, _, test_set = random_split(data_set, [train_size, valid_size, test_size], generator=torch.Generator().manual_seed(seed))
    print(f'training size: {train_size:d}, validation size: {valid_size:d}, test size: {test_size:d}')

    test_loader = DataLoader(test_set, batch_size=args.batch_size, shuffle=False)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    model = restore_checkpoint(args.model, device)

    # evaluate test data
    loss, acc = model.evaluation(test_loader, device=device)
    print(f'test data set loss: {loss:.5f}, acc: {acc:.5f}')

    live = Live(os.path.join("evaluation", 'test'))
    live.log('accuracy', acc)
    live.log('loss', loss)


if __name__ == '__main__':
    evaluate()

# python /app/src/evaluate.py -m /app/checkpoints/image_qty_classifier_best-acc.pt -d /app/data/train -e /app/evaluation
